### Run jupyter notebook

`sudo docker-compose up` 

then copy token from terminal and open 127.0.0.1:8888 in browser... past token

### Rebuild main image 

`sudo docker-compose build`


#### reading about python collections (rus)

[1/4: классификация, общие подходы и методы, конвертация](https://m.habr.com/post/319164/)

[2/4: индексирование, срезы, сортировка](https://m.habr.com/post/319200/)

[3/4: объединение коллекций, добавление и удаление элементов](https://m.habr.com/post/319876/)

[4/4: Все о выражениях-генераторах, генераторах списков, множеств и словарей](https://m.habr.com/post/320288/)


#### about collections and etc (en)

[HowTo/Sorting#Key_Functions](https://wiki.python.org/moin/HowTo/Sorting#Key_Functions)

[HowTo/Sorting#Sort_Stability_and_Complex_Sorts](https://wiki.python.org/moin/HowTo/Sorting#Sort_Stability_and_Complex_Sorts)

[Iterables vs. Iterators vs. Generators](https://nvie.com/posts/iterators-vs-generators/)

[Want to understand Python’s comprehensions? Think in Excel or SQL.](http://blog.lerner.co.il/want-to-understand-pythons-comprehensions-think-like-an-accountant/)

[Python List Comprehensions: Explained Visually](https://treyhunner.com/2015/12/python-list-comprehensions-now-in-color/)

[Understanding nested list comprehensions in Python](http://blog.lerner.co.il/understanding-nested-list-comprehensions-in-python/)

##### Complexity

[the time-complexity (aka "Big O" or "Big Oh")](https://wiki.python.org/moin/TimeComplexity)

[Complexity of Python Operations](https://www.ics.uci.edu/~pattis/ICS-33/lectures/complexitypython.txt)
FROM python:3.7.0-stretch
# FROM python:3.6.6-stretch
RUN echo 'deb http://deb.debian.org/debian stretch non-free' >> /etc/apt/sources.list
RUN apt-get update&&apt-get install apt-utils git libzmq3-dev -y

WORKDIR /notebooks
COPY requirements.txt /notebooks/requirements.txt

RUN pip install -r /notebooks/requirements.txt
# bash kernel for jupyter
RUN python -m bash_kernel.install

# golang kernel for jupyter
RUN wget https://dl.google.com/go/go1.11.2.linux-amd64.tar.gz \
    && tar -xvf go1.11.2.linux-amd64.tar.gz \
    && mv go /usr/local \
    && export GOROOT=/usr/local/go \
    && export PATH=$GOPATH/bin:$GOROOT/bin:$PATH \
    && go get -u github.com/gopherdata/gophernotes \
    && mkdir -p /usr/local/share/jupyter/kernels/gophernotes \
    # && cp /root/go/src/github.com/gopherdata/gophernotes/kernel/* /usr/local/share/jupyter/kernels/gophernotes \
    && echo '{"argv": ["/root/go/bin/gophernotes","{connection_file}"], "display_name": "Go", "language": "go", "name": "go"}' > /usr/local/share/jupyter/kernels/gophernotes/kernel.json \
    && go version

RUN echo 'export GOROOT=/usr/local/go' >> /root/.bashrc \
    && echo 'export GOPATH=/root/go' >> /root/.bashrc \
    && echo 'export PATH=$GOPATH/bin:$GOROOT/bin:$PATH' >> /root/.bashrc 

EXPOSE 8888
ENTRYPOINT ["jupyter", "notebook", "--ip=0.0.0.0", "--allow-root", "--no-browser"]
